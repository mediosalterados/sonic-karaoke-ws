<?php
/**
 * Get BLOB Data from Database and ouput
 */
if(isset($_GET['id'])){
  $id = $_GET['id'];
  require_once __DIR__ . "/db.php";
  $sql = $dbh->prepare("SELECT `audio`, LENGTH(`audio`) FROM `uploads` WHERE `id` = ?");
  $sql->execute(array($_GET['id']));
  $result = $sql->fetch();

  $audio = $result[0];
  $file_location = "../audios/".$id.".wav";
 
  file_put_contents($file_location, $audio);
}
