/*
 Created on : Jul 11, 2015, 2:36:57 AM
 Author     : ZER0
 */
var sliders = [];
var friendsIDarray = [];
var user_friend_list;
var challenge_url;
$(document).ready(function () {

    $.ajaxSetup({cache: true});
    $.getScript('//connect.facebook.net/es_MX/sdk.js', function () {
        FB.init({
            appId: '948687841944677',
            version: 'v2.7' // or v2.1, v2.2, v2.3, ...
        });
    });
    if ($(".slider-content").length) {
        $(".slider-content").each(function () {
            sliders.push($(this).bxSlider({
                slideMargin: $(this).data('slide-margin') || 0,
                moveSlides: $(this).data('move-slides') || 1,
                mode: $(this).data('mode') || 'horizontal',
                slideWidth: $(this).data('slide-width') || 0,
                maxSlides: $(this).data('max-slides') || 1,
                minSlides: $(this).data('max-slides') || 1,
                auto: hasData($(this).data('auto')) ? $(this).data('auto') : true,
                pager: hasData($(this).data('pager')) ? $(this).data('pager') : false || true,
                controls: hasData($(this).data('controls')) ? $(this).data('controls') : false || true,
                speed: $(this).data('speed') || 800,
                responsive: hasData($(this).data('responsive')) ? $(this).data('responsive') : false || true
            }));
        });
    }

    $(".btn-continue").click(function () {
        sliders[0].goToNextSlide();
    });
    if ($(".fancybox").length) {
        $(".fancybox").fancybox();
    }


    $('a.do-share').click(function () {
        shareSite();
    });
    $('form.ajax-submit').submit(function () {

        if (!challenge_url) {
            var form = $(this);
            $.ajax({
                dataType: 'json',
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    if (response.status === 'ok') {
                        challenge_url = response.url_challenge;
//                    meTaggableFriends();
//
//
//                    FB.api(
//                            'me/objects/sonic-karaoke:challenge',
//                            'post',
//                            {'object': {
//                                    'og:url': 'http://samples.ogp.me/957553574391437',
//                                    'og:title': 'Sample Challenge',
//                                    'og:type': 'sonic-karaoke:challenge',
//                                    'og:image': 'https://fbstatic-a.akamaihd.net/images/devsite/attachment_blank.png',
//                                    'og:description': '',
//                                    'fb:app_id': '948687841944677'
//                                }},
//                    function (response) {
//                        console.log(response);
//                    }
//                    , {access_token: access_token});


                        var object = {
                            method: 'feed',
                            link: challenge_url,
                            caption: 'Entra al reto...',
                        };
                        sharePost(object);
                    }
                }
            });
        } else {
            var object = {
                method: 'feed',
                link: challenge_url,
                caption: 'Entra al reto...',
            };
            sharePost(object);
        }

        return false;
    });
    $('.a-submit').click(function () {
        var f = $(this).parents('form');
        f.submit();
    });
});
function hasData(data) {
    return  typeof (data) !== undefined && data !== null ? true : false;
}


function fontSize(mViewport, dViewport) {
    mViewport = mViewport || 320;
    dViewport = dViewport || 2015;
    var containerWidth = $("html").width();
    var htmlFS;
    if ($("html").hasClass('is-mobile')) {
        htmlFS = containerWidth * 10 / mViewport;
    } else {
        htmlFS = containerWidth * 10 / dViewport;
    }
    $("html").css({"font-size": htmlFS});
}

$('.circle-content-genero').click(function () {
    $('.circle-content-genero').removeClass('active');
    $(this).find('input[type="radio"]').prop('checked', true);
    $(this).addClass('active');
});
function meTaggableFriends() {


    FB.api(
            "/me/taggable_friends",
            function (response) {
                if (response && !response.error) {
                    /* handle the result */
                    console.log(response)

                    for (var i = 0; i < response.data.length; i++) {

                        var data = response.data;
                        friendsIDarray.push(data[i].id);
                    }
                    user_friend_list = friendsIDarray.join();
                    console.log(user_friend_list);
                }
            },
            {access_token: access_token});
}


function shareSite() {
    FB.ui({
        method: 'share',
        href: 'https://www.sonickaraoke.mx',
    }, function (response) {
    }, {access_token: access_token});
}

function sharePost(object) {
    if (typeof (access_token) !== 'undefined') {
        FB.ui(object, postResponse, {access_token: access_token});
    } else {
        FB.ui(object, postResponse);
    }
}

function postResponse(response) {
    if (response.post_id) {
        window.location = retaste_url;
    } else {
        alert('error al compartir');
    }
}