<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="score")
 * @ORM\Entity
 *  
 */
class Score {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="score", type="float") */
    protected $score;

    /** @ORM\Column(name="genero", type="string") */
    protected $genero;

    /**
     * @var \Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="scores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ChallengeResult", mappedBy="score")
     */
    private $challenges;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return Score
     */
    public function setScore($score) {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * Set usuario
     *
     * @param \AdminBundle\Entity\Usuario $usuario
     *
     * @return Score
     */
    public function setUsuario(\AdminBundle\Entity\Usuario $usuario = null) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AdminBundle\Entity\Usuario
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->challenges = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add challenge
     *
     * @param \AdminBundle\Entity\ChallengeResult $challenge
     *
     * @return Score
     */
    public function addChallenge(\AdminBundle\Entity\ChallengeResult $challenge) {
        $this->challenges[] = $challenge;

        return $this;
    }

    /**
     * Remove challenge
     *
     * @param \AdminBundle\Entity\ChallengeResult $challenge
     */
    public function removeChallenge(\AdminBundle\Entity\ChallengeResult $challenge) {
        $this->challenges->removeElement($challenge);
    }

    /**
     * Get challenges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChallenges() {
        return $this->challenges;
    }


    /**
     * Set genero
     *
     * @param string $genero
     *
     * @return Score
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }
}
